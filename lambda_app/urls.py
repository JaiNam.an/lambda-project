from . import views
from django.urls import path


urlpatterns = [
    path('', views.lambda_home_page, name='lambda_home')
]
