import logging
from boto3 import client
from django.shortcuts import render
from botocore.exceptions import ClientError


bucket_name = 'lambda-project-upload'


# Create your views here.
def send_file_to_s3(file, filename):
    try:
        s3_client = client('s3')
        s3_client.upload_fileobj(file, bucket_name, filename)

    except ClientError as e:
        logging.error(e)


def lambda_home_page(request):
    # return HttpResponse("<h1>Welcome to Lambda's page</h1>")
    if request.method == 'POST':
        file = request.FILES['file']
        filename = file.name
        send_file_to_s3(file, filename)
        return render(request, "upload.html", context={'file_name': filename})

    return render(request, "index.html")
